<?php

namespace App\Http\Controllers;

use App\Listeners\AddActionListener;
use Illuminate\Console\Scheduling\Event;
use Illuminate\Http\Request;
use App\Secretary;
use DateTime;
use DB;
use PhpParser\Node\Expr\Eval_;

class secretaryController extends Controller
{
    protected $url = 'https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5';
    protected $currency;
    protected $currency_cost;
    protected $currencies;

    function __construct()
    {
        $currencies_dbs = DB::select("SELECT * FROM currencies");
        $this->currencies = $currencies_dbs;
        foreach ($currencies_dbs as $currencies_db){
            if($currencies_db->default_currency == "1"){
                $this->currency = $currencies_db->name;
            }
        }


        // cut usd cost via privat bank api
        $currencies = [];
        $currencies_value = json_decode(file_get_contents($this->url));
        foreach ($currencies_value as $currency){
            $currencies[$currency->ccy] = $currency;
        }
        $this->currency_cost = $currencies[$this->currency]->sale;
    }
// загрузка главной страницы
    public function index(){
        $operations = Secretary::all();
        //total cost
        $total_out = 0;
        $total_in = 0;
        $operations = $this->currencyExchange($operations);
        foreach ($operations as $operation){

            if($operation->type == 'outcome'){
                $total_out += $operation->value;
            }elseif($operation->type == 'income'){
                $total_in += $operation->value;
            }
        }
        return view('secretary', ['operations' => $operations, 'total_out' => $total_out,'total_in' => $total_in, 'currency_cost' =>$this->currency_cost, 'currencies' => $this->currencies]);

    }
// отмена фтльтра
    public function filterOff(){
        $operations = Secretary::all();
        //total cost
        $total_out = 0;
        $total_in = 0;
        $operations = $this->currencyExchange($operations);
        foreach ($operations as $operation){

            if($operation->type == 'outcome'){
                $total_out += $operation->value;
            }elseif($operation->type == 'income'){
                $total_in += $operation->value;
            }
        }
        return response()->json(['operations' => $operations, 'total_out' => $total_out,'total_in' => $total_in, 'currency_cost' =>$this->currency_cost]);

    }
//    применения фильтра
    public function getOperationAjax(Request $request){
        $form_data = $request->all();
        $form_data = json_decode($form_data['data']);
        $date_start = new DateTime($form_data->start);
        $date_end = new DateTime($form_data->end);
        $start = $date_start->format('Y-m-d H:i:s');
        $end = $date_end->format('Y-m-d H:i:s');
        $operations =DB::select("SELECT * FROM secretary WHERE created_at >= ? AND created_at <= ?", [$start, $end] );
        $total_out = 0;
        $total_in = 0;
        $operations = $this->currencyExchange($operations);
        foreach ($operations as $operation){
            if($operation->type == 'outcome'){
                $total_out += $operation->value;
            }elseif($operation->type == 'income'){
                $total_in += $operation->value;
            }
        }
        return response()->json(['operations' => $operations, 'total_out' => $total_out,'total_in' => $total_in, 'currency_cost' =>$this->currency_cost]);

    }
//    обновления и добавления записей
    public function editAndAdd(Request $request){

        $form_data = $request->all();
        $form_data = $form_data["data"];

        if(isset($form_data['id']) && $form_data['id'] != null){
            $operation = Secretary::find($form_data['id']);
            $operation->name = $form_data['name'];
            $operation->description = $form_data['description'];
            $operation->value = $form_data['value'];
            $operation->type = $form_data['type'];
            if(strlen($form_data['currency']) > 0){
                $operation->currency = $form_data['currency'];
            }
            $operation->save();
        }else{
            $operation =  new Secretary;
            $operation->name = $form_data['name'];
            $operation->description = $form_data['description'];
            $operation->value = $form_data['value'];
            $operation->type = $form_data['type'];
            if(strlen($form_data['currency']) > 0){
                $operation->currency = $form_data['currency'];
            }else{
                $operation->currency = 'USD';
            }
            $operation->save();
        }

        //temp code
            $operations = Secretary::all();

        //total cost
        $total_out = 0;
        $total_in = 0;
        $operations = $this->currencyExchange($operations);
        foreach ($operations as $operation){
            if($operation->type == 'outcome'){
                $total_out += $operation->value;
            }elseif($operation->type == 'income'){
                $total_in += $operation->value;
            }
        }

        event( new AddActionListener($operation));
        return response()->json(['operations' => $operations, 'total_out' => $total_out,'total_in' => $total_in, 'currency_cost' =>$this->currency_cost]);
    }
//    удаления записей
    public function delete($id){
        $operation = Secretary::find($id);
        $operation->delete();
        //temp code
        $operations = Secretary::all();

        //total cost
        $total_out = 0;
        $total_in = 0;
        $operations = $this->currencyExchange($operations);
        foreach ($operations as $operation){
            if($operation->type == 'outcome'){
                $total_out += $operation->value;
            }elseif($operation->type == 'income'){
                $total_in += $operation->value;
            }
        }

        return response()->json(['operations' => $operations, 'total_out' => $total_out,'total_in' => $total_in, 'currency_cost' =>$this->currency_cost]);

    }
//    вспомогательна функция для конвертации валют
    protected function currencyExchange($objects){
        foreach ($objects as $key => $object){
            if($object->currency == 'USD'){
                $objects[$key]->value_uah = $objects[$key]->value * $this->currency_cost;
            }elseif($object->currency == 'UAH'){
                $objects[$key]->value_uah = $objects[$key]->value;
                $objects[$key]->value = round($objects[$key]->value / $this->currency_cost, 2);
            }
        }
        return $objects;
    }

}
