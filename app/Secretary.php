<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Secretary extends Model
{
    protected $table = "secretary";
    protected $primaryKey = 'id';
    public $timestamps = true;
    protected $fillable = ['value','name','currency', 'description'];
}
