<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Log;
use App\Secretary;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Secretary::created(function(Secretary $secretary){
            Log::info('Action name ', ['Action name' => $secretary->action_name]);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
