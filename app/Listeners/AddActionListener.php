<?php

namespace App\Listeners;

use App\Events\onAddAction;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Log;
class AddActionListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  onAddAction  $event
     * @return void
     */
    public function handle(onAddActoin $event)
    {
        Log::info('Action name ', ['Action name' => $event->action_name]);

    }
}
