@include('header')
<h1>All operations</h1>

@if(count($operations) > 0)
    Select for period: from <input type="date" class="start form-control" value="2017-07-30"> to
    <input value="2017-08-01" type="date" class="end form-control"> <br><span class="filter btn btn-default">Use filter</span><br>
    <br><span class="filter_off btn btn-default">Turn off filter</span><br>
<br>
    <p class="alert-info"><span >* </span> If you want add new operation just use form on bottom of page.</p>
    <p class="alert-info"><span >* </span> If yuu want edit existing operation click on edit button and use for bellow.</p>
    <table class="table">
    <tr>
        <td>№</td>
        <td>Name</td>
        <td>Cost usd</td>
        <td>Cost UAH</td>
        <td>currency</td>
        <td>type</td>
        <td>Created</td>
        <td>Actions</td>
        <td>Description</td>

    </tr>
        <tbody class="main_table">
    @foreach($operations as $operation)
    <tr class="operation_tr" data-id="{{$operation->id}}">
        <td >{{$loop->iteration}}</td>
        <td class="operation_name">{{$operation->name}}</td>
        <td class="operation_value">{{$operation->value}} </td>
        <td class="operation_value_uah">{{$operation->value_uah}}</td>
        <td class="operation_currency">{{$operation->currency}}</td>
        <td class="operation_type">{{$operation->type}}</td>
        <td >{{$operation->created_at}}</td>
        <td><span class="edit">Edit</span>/<span class="delete" data-operation="{{$operation->id}}">delete</span></td>
        <td class="operation_description">{{$operation->description}}</td>
    </tr>
    @endforeach
    <tr>
        <td>Total</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td>{{$total_out * $currency_cost}} Outcome</td>
        <td>{{$total_in * $currency_cost}} Income</td>
    </tr>
        </tbody>
</table>
@endif

<form action="{{action('secretaryController@editAndAdd')}}" class="edit_form" method="POST">
    <div class="form-group">
        <label for="name">Name:</label>
        <input class="form-control" id="name" type="text" name="name" placeholder="name">
        <label for="description">Description:</label>
        <input class="form-control" id="description" type="text" name="description" placeholder="description">
        <label for="value">Value:</label>
        <input class="form-control" id="value" type="text" name="value" placeholder="value">
        <label for="currency">Currency:</label>
        @if(count($currencies) > 0)
            <select class="form-control" id="currency" type="select" name="currency">
            @foreach($currencies as $currency)
                    <option value="{{$currency->name}}">{{$currency->name}}</option>
            @endforeach
            </select>
        @endif
        <label for="type">Type:</label>
        <select class="form-control" id="type" type="select" name="type">
            <option value="income">Income (USD)</option>
            <option value="outcome">Outcome (USD)</option>
        </select>
        <input class="form-control id_operation" name="id" type="hidden" value>
        <input class="form-control" type="hidden" name="_token" value="{{csrf_token()}}"/>
        <br>
        <input class="form-control btn btn-default" type="submit" value="Edit/Add">
    </div>
</form>
@include('footer')