<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['uses'=>'secretaryController@index', 'as' => 'home']);
Route::post('/', ['uses'=>'secretaryController@editAndAdd', 'as' => 'editAndAdd']);
Route::post('/filter', ['uses'=>'secretaryController@getOperationAjax', 'as' => 'filter']);
Route::post('/filterOff', ['uses'=>'secretaryController@filterOff', 'as' => 'filterOff']);
Route::get('/delete/{id}', ['uses'=>'secretaryController@delete', 'as' => 'delete']);
