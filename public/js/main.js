var secretary = (function() {

    return { // методы доступные извне
        init:function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('[name="_token"]').val()
                }
            });

        },
        setUpdateData: function(elem) {
                var id = elem.data('id');
                var value = elem.find(".operation_value").text();
                var name = elem.find(".operation_name").text();
                var currency = elem.find(".operation_currency").text();
                var type = elem.find(".operation_type").text();
                var description = elem.find(".operation_description").text();
                $('.id_operation').val(id);
                $('#name').val(name);
                $('#description').val(description);
                $('#value').val(value);
                $('#currency').val(currency);
                $('#type').val(type);
        },
        useFilter: function() {
            var token = $('input[name="_token"]').val();
            var start = $(".start").val();
            var end = $(".end").val();
            var data = {'start': start, 'end': end};
            data = JSON.stringify(data);
            $.ajax({
                url:'filter',
                data:{
                    data:data
                },
                dataType: 'json',
                type:"post",
                success:function (data) {
                    secretary.update(data);
                },
                error:function () {
                    console.log("error");
                }
            });

        },
        filterOff: function() {
            $.ajax({
                url:'filterOff',
                dataType: 'json',
                type:"post",
                success:function (data) {
                    secretary.update(data);
                },
                error:function () {
                    console.log("error");
                }
            });

        },
        updateOperation: function(data) {
            $.ajax({
                url:'',
                data:{
                    data:data
                },
                dataType: 'json',
                type:"post",
                success:function (data) {
                    secretary.update(data);
                },
                error:function () {
                    console.log("error");
                }
            });
        },
        deleteOperation:function(id){
            $.ajax({
                url:'delete/'+id,
                dataType: 'json',
                type:"GET",
                success:function (data) {
                    secretary.update(data);
                },
                error:function () {
                    console.log("error");
                }
            });
        },
        update:function (data) {
            $(".operation_tr").remove();
            var str = '';
            var operations = data.operations;
            var increment = 1;
            for(var i=0; i < operations.length; i++){
                str += '<tr data-id="'+operations[i].id+'">';
                str += '<td>'+increment+'</td>';
                str += '<td class="operation_name">'+operations[i].name+'</td>';
                str += '<td class="operation_value">'+operations[i].value+'</td>';
                str += '<td class="operation_value_uah">'+operations[i].value_uah+'</td>';
                str += '<td class="operation_currency">'+operations[i].currency+'</td>';
                str += '<td class="operation_type">'+operations[i].type+'</td>';
                str += '<td>'+operations[i].updated_at+'</td>';
                str += '<td><span class="edit">Edit</span>/<span class="delete" data-operation="'+operations[i].id+'">delete</span></td>';
                str += '<td class="operation_description">'+operations[i].description+'</td>';
                str += '</tr>';
                ++increment;
            }
            var table_footer = '';
            table_footer += '<tr><td>Total</td><td></td><td></td><td></td><td></td><td></td><td></td>' +
                '<td>'+data.total_out+' Outcome</td>'+
                '<td>'+data.total_in+' Income</td></tr>';
            $(".main_table").html(str+table_footer);

        }
    }
}());



$(document).ready(function () {

    secretary.init();
    //заполнения формы текушими значениями

    $('body').on('click', '.edit', function () {
        var elem = $(this).closest('tr');
        secretary.setUpdateData(elem);
    });

    $("body").on('click', ".delete", function () {
        var id = $(this).data("operation");
        secretary.deleteOperation(id);
        return false;
    })
    //выборка с базы даных с учетом фильтра по дате

    //add new or update existing

    $(".edit_form").submit(function () {
        var data = {
            'name': $(this).find("#name").val(),
            'value': $(this).find("#value").val(),
            'id':$(this).find(".id_operation").val(),
            'currency':$(this).find("#currency").val(),
            'type': $(this).find("#type").val(),
            'description': $(this).find("#description").val(),
        };
        secretary.updateOperation(data);
        return false;
    });

    $(".filter").on('click', function () {
        secretary.useFilter();
    });
    $(".filter_off").on('click', function () {
        secretary.filterOff();
    });
})